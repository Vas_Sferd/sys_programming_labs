/*
 * main.c
 *
 * Copyright 2021 Vas Sferd <vassferd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <assert.h>
#include <conio.h>
#include <fileapi.h>
#include <locale.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include <strings.h>
#include <buffer.h>
#include <usize_box.h>

//#define NDEBUG

#ifndef NDEBUG
#define MARK(S) printf("[Point %s:%d] %s\n", __FILE__, __LINE__, S)
#else
#define MARK(S)
#endif

/*
 * Функция возвращает длину строки или ноль
 * Нескольно "return" в коде функции!
 */
size_t
fread_line_to_buff(
        const HANDLE file_handle,
        string_buffer *mut_buff,
        BOOL *mut_is_ok
        ) {
    BOOL mut_is_file_finished = FALSE;

    MARK("Начало чтения файла");

    do {
        const size_t slice_len = 256;
        DWORD mut_readed_chars_count = 0;
        mut_string_slice buff_slice = get_mut_string_slice(mut_buff, slice_len);

        if (!mut_is_file_finished) {
            *mut_is_ok = ReadFile(
                file_handle,
                buff_slice.chars,
                buff_slice.max_len * sizeof (char),
                &mut_readed_chars_count,
                NULL
            );
        }

        if (*mut_is_ok) {
            if (mut_readed_chars_count < slice_len) {
                mut_is_file_finished = TRUE;
                buff_slice.chars[mut_readed_chars_count++] = '\n';

                MARK("Файл считан до конца");
            }

            string_buffer_register_writed_data(mut_buff, mut_readed_chars_count);

            for (size_t i = 0; i < mut_readed_chars_count; ++i) {
                if (buff_slice.chars[i] == '\n') {
                    return mut_buff->cur_position - mut_readed_chars_count + i;
                }
            }
        }
    } while (mut_is_ok != 0);

    MARK("Произошла непредвиденная ошибка");
    return 0;
}

string
fread_line(
        const HANDLE file_handle,
        string_buffer *mut_buff,
        BOOL *mut_is_ok,
        panic_provider p_provider
        ) {
    size_t line_len = fread_line_to_buff(
                file_handle,
                mut_buff,
                mut_is_ok
                );

    if (*mut_is_ok) {
        const string result = string_buffer_read_string(*mut_buff, line_len);
        string_buffer_clear_for(mut_buff, line_len);

#ifndef NDEBUG
        MARK("Новая строка");
        printf("[String] %s\n", result.chars);
#endif

        return result;
    } else {
        send_panic(p_provider);
    }
}

void
fskip_line(
        const HANDLE file_handle,
        string_buffer *mut_buff,
        BOOL *mut_is_ok,
        panic_provider p_provider
        ) {
    size_t line_len = fread_line_to_buff(
                file_handle,
                mut_buff,
                mut_is_ok
    );

    if (*mut_is_ok) {
        string_buffer_clear_for(mut_buff, line_len);
    } else {
        send_panic(p_provider);
    }
}

opt_strings_box
fread_the_lines(
        const char *const filename,
        const usize_array_box arr
        ) {
    const usize_array_box sorted_arr = sort_usize_array_box(arr);

    HANDLE source_file = CreateFile(
                filename,
                GENERIC_READ,
                FILE_SHARE_READ,
                NULL,
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL,
                NULL
    );

    if (source_file == INVALID_HANDLE_VALUE) {
        return opt_strings_box_none();
    }

    MARK("Файл открылся нормально");

    size_t mut_line_number = 1;
    string *restrict strs = calloc(sorted_arr.count, sizeof (string));
    string_buffer buff = create_string_buffer(256);

    BOOL mut_is_ok = TRUE;
    for (size_t i = 0; i < sorted_arr.count; ++i) {
        while (mut_line_number < sorted_arr.nums[i]) {
            fskip_line(
                        source_file,
                        &buff,
                        &mut_is_ok,
                        GET_PANIC_PROVIDER(panic)
            );

            ++mut_line_number;

            MARK("Пропустили еще одну строчку");
        }
        strs[i] = fread_line(
                    source_file,
                    &buff,
                    &mut_is_ok,
                    GET_PANIC_PROVIDER(panic)
        );

        ++mut_line_number;

        MARK("Ура! Записали строку!");
    }

    free_usize_array_box(sorted_arr);
    free_string_buffer(buff);

    CloseHandle(source_file);

    MARK("Выходим после освобождения ресурсов");

    return opt_strings_box_some(wrap_to_str_line_box(strs, arr.count));
}

usize_array_box
parse_from_chars_array(
        const char *const *const chars_arr,
        const size_t count
        ) {
    size_t *restrict nums = calloc(count, sizeof (size_t));
    for (size_t i = 0; i < count; ++i) {
        nums[i] = (size_t)(unsigned)atoi(chars_arr[i]);
    }

    return wrap_to_usize_array_box(nums, count);
}

void
fwrite_string(
        const HANDLE file_handle,
        const string *const str,
        string_buffer *mut_buff,
        BOOL *mut_is_ok,
        panic_provider p_provider
        ) {
    DWORD mut_readed_chars_count = 0;
    mut_string_slice buff_slice = get_mut_string_slice(mut_buff, str->len);

    *mut_is_ok = ReadFile(
                file_handle,
                buff_slice.chars,
                buff_slice.max_len * sizeof (char),
                &mut_readed_chars_count,
                NULL
    );

    if (!*mut_is_ok) {
        send_panic(p_provider);
        return;
    } else {
        string_buffer_register_writed_data(mut_buff, mut_readed_chars_count);
    }

    *mut_is_ok = *mut_is_ok = SetFilePointer(
                file_handle,
                -mut_readed_chars_count,
                NULL,
                FILE_CURRENT
    );

    if (!*mut_is_ok) {
        send_panic(p_provider);
        return;
    } else {
        DWORD mut_writed_chars_count = 0;

        *mut_is_ok = WriteFile(
                    file_handle,
                    str->chars,
                    str->len * sizeof (char),
                    &mut_writed_chars_count,
                    NULL
        );

        assert(mut_writed_chars_count >= mut_readed_chars_count);
    }
}

void
fflush_string_buffer(
        const HANDLE file_handle,
        string_buffer *mut_buff,
        BOOL *mut_is_ok,
        panic_provider p_provider) {
    DWORD mut_readed_chars_count = 0;
    do {
        mut_string_slice buff_slice = get_mut_string_slice(mut_buff, 256);
        *mut_is_ok = ReadFile(
                    file_handle,
                    buff_slice.chars,
                    buff_slice.max_len * sizeof (char),
                    &mut_readed_chars_count,
                    NULL
        );

        DWORD mut_writed_chars_count = 0;
        if (*mut_is_ok) {
            if (mut_readed_chars_count != 0) {
                string_buffer_register_writed_data(mut_buff, mut_readed_chars_count);

                *mut_is_ok = SetFilePointer(
                            file_handle,
                            -mut_readed_chars_count,
                            NULL,
                            FILE_CURRENT
                );

                if (!*mut_is_ok) {
                    send_panic(p_provider);
                }

                MARK("Переместили указатель назад после записи в буфер");

                *mut_is_ok = WriteFile(
                            file_handle,
                            mut_buff->chars,
                            mut_readed_chars_count * sizeof (char),
                            &mut_writed_chars_count,
                            NULL
                );

                assert(mut_readed_chars_count <= mut_writed_chars_count);

                string_buffer_clear_for(mut_buff, (size_t)(unsigned)mut_readed_chars_count);
            } else if (mut_buff->cur_position > 0) {

                MARK("Печатаем оставшиеся символы из буфера");

                *mut_is_ok = WriteFile(
                            file_handle,
                            mut_buff->chars,
                            mut_buff->cur_position * sizeof (char),
                            &mut_writed_chars_count,
                            NULL
                );

                string_buffer_clear(mut_buff);
            }
        } else {
            send_panic(p_provider);
        }
    } while (mut_readed_chars_count != 0 && mut_buff->cur_position != 0);
}

void
fwrite_strings_box(
        const HANDLE file_handle,
        const strings_box *const box,
        string_buffer *mut_buff,
        BOOL *mut_is_ok,
        panic_provider p_provider
        ) {
    for (size_t i = 0; i < box->count; ++i) {
        fwrite_string(
                    file_handle,
                    &box->strs[i],
                    mut_buff,
                    mut_is_ok,
                    p_provider
        );
    }

    if (!*mut_is_ok) {
        send_panic(p_provider);
        return;
    }

    fflush_string_buffer(
                file_handle,
                mut_buff,
                mut_is_ok,
                p_provider
    );
}


void
fwrite_to_line(
        const char *const filename,
        const strings_box strs_box,
        const size_t target_line_number,
        panic_provider p_provider
        ) {
    HANDLE target_file = CreateFile(
            filename,
            GENERIC_WRITE,
            0,
            NULL,
            OPEN_ALWAYS,
            FILE_ATTRIBUTE_NORMAL,
            NULL
    );

    if (target_file == INVALID_HANDLE_VALUE) {
        send_panic(p_provider);
    }

    MARK("Файл был открыт удачно");

    string_buffer buff = create_string_buffer(256);
    BOOL mut_is_ok = TRUE;
    size_t mut_line_number = 0;

    while (mut_line_number++ < target_line_number) {
        fskip_line(
                target_file,
                &buff,
                &mut_is_ok,
                GET_PANIC_PROVIDER(panic)
        );

        MARK("Пропустили еще одну строчку");
    }

    if (!mut_is_ok) {
        send_panic(p_provider);
    }

    fwrite_strings_box(
                target_file,
                &strs_box,
                &buff,
                &mut_is_ok,
                p_provider
    );

    if (!mut_is_ok) {
        send_panic(p_provider);
    }

    free_str_line_box(strs_box);
    free_string_buffer(buff);

    CloseHandle(target_file);
}


int
main(
        const int argc,
        const char *const argv[]
        ) {
    setlocale(LC_ALL, "ru_RU.UTF-8");

    if (argc < 5) {
        puts("Ожидалось больше аргументов коммандной строки. Программа завершена");
        return -1;
    }

    const char *const source_file_name = argv[1];
    const char *const target_file_name = argv[2];

    MARK("Считали первые два аргумента. Знаем имена файлов");

    const size_t string_count = (size_t)(unsigned)atoi(argv[3]);
    if ((size_t)(unsigned)argc != 5 + string_count) {
        printf("Неверное число аргументов. Ожидалось: %llu\n", 4 + string_count);
        return -2;
    }

    const usize_array_box line_numbers = parse_from_chars_array(&argv[4], string_count);
    const size_t target_line_number = (size_t)(unsigned)atoi(argv[argc - 1]);

    MARK("Получили данные о строках, которые необходимо считать и записать");

    const opt_strings_box opt_strs_box = fread_the_lines(source_file_name, line_numbers);
    const strings_box strs_box = UNWRAP_STRING_BOX(opt_strs_box);

#ifndef NDEBUG

    MARK("Вывод сохраненных значений");

    for (size_t i = 0; i < strs_box.count; ++i) {
        printf("[DEBUG] [%llu] %s\n", i, strs_box.strs[i].chars);
    }
#endif

    MARK("Дошли до записи в файл");

    fwrite_to_line(
                target_file_name,
                strs_box,
                target_line_number,
                GET_PANIC_PROVIDER(panic)
    );

    MARK("Записали в файл");

    return 0;
}
