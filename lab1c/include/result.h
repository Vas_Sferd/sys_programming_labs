#ifndef __RESULT_H__
#define __RESULT_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>

enum ErrorCode {
    UNHANDLED_ERROR = -1,
    SUCCESS = 0,
    TRY_ERROR
};

struct CodePosition {
    const char *_file;
    const char *_func;
    const int _line;
};

#define CODE_POSITION_HERE ((struct CodePosition) {__FILE__, __func__, __LINE__})

struct Error {
    struct CodePosition _position;
    struct Error *_cause;
};

struct Ok {
    char *_typename;
    char *_value;
};

#define OK_GET(TYPE, VALUE) _GENERIC(VALUE, \
    struct Ok: (strcmp((VALUE)._typename, #TYPE) \
        ? (TYPE)*(VALUE)._value \
        : _panic(CODE_POSITION_HERE, ErrorCode.UNHANDLED_ERROR, "Wrong TYPE paremetr in OK_GET Macros: " #TYPE " not in this \"struct Ok\""), \
    _panic(CODE_POSITION_HERE, ErrorCode.UNHANDLED_ERROR, "Wrong type in OK_GET Macros: " #VALUE " must be a \"struct Ok\"") \
)

union OkError {
    struct Ok _ok;
    struct Error _err;
};

struct Result {
    int code;
    union OkError _ok_or_err;
};

noreturn void
_panic(struct CodePosition code_point, int code, char const* message) {
    fprintf(stderr, "[Error] %s\n\tPanic handled at function %s (%s:%d)",
            message,
            code_point._func,
            code_point._file,
            code_point._line
            );
    exit(code);
}

static inline char*
_boxing_or_fail(struct CodePosition code_point, char const* value, size_t size) {
    char *ptr = malloc(size);
    if (ptr == NULL) {
        _panic(code_point, -1, "Memory allocation Failed");
    }
    memcpy(ptr, value, size);
    return ptr;
}

#define BOXING_OR_FAIL(VALUE) _boxing_or_fail(CODE_POSITION_HERE, (char const*)&(VALUE), sizeof(VALUE))

struct Result
result_error(struct CodePosition code_point, int code, struct Error *const cause) {
    if (code == 0) {
        _panic(CODE_POSITION_HERE, -1, "0 are not an error code. ");
    }

    struct Error *_cause = (struct Error *)BOXING_OR_FAIL(*cause);
    struct Error error = { ._position = code_point, ._cause = _cause };
    union OkError ok_error = {._err = error};
    struct Result result = {.code = code, ._ok_or_err = ok_error};
    return result;
}

struct Result
result_ok(char *_typename, char *_value) {
    union OkError ok_error = {._err = error};
    struct Result result = {.code = code, ._ok_or_err = ok_error};
    return result;
}


#define UNWRAP(EXPR_TYPE, EXPR)

#define TRY(EXPR_TYPE, EXPR) _GENERIC((EXPR), \
    struct Result: ((EXPR).code == 0 ? (EXPR)._ok_or_err._ok._value) : ({ return  }; (struct Result){0};),\
    default: _panic(CODE_POSITION_HERE, ErrorCode.UNHANDLED_ERROR, "Wrong type in TRY Macros: " #EXPR " must be a \"struct Result\"") \
)

#endif // __RESULT_H__
