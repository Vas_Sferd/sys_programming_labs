#ifndef __STRINGS_H__
#define __STRINGS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * { Ресурс }
 * Владелец "zero-terminated" ASCII строки с числом символов в ней
 */
typedef struct {
    char *restrict chars;
    size_t len;
} string;


/*
 * Только для ASCII "zero-terminated" строк
 */
string
wrap_to_string(
        char *restrict ascii_chars,
        size_t len
        ) {
    string resource = {0};
    resource.chars = ascii_chars;
    resource.len = len;
    return resource;
}

/*
 * Только для ASCII "zero-terminated" строк.
 */
string
create_string(char *ascii_chars) {
    string resource = {0};
    resource.len = strlen(ascii_chars);
    resource.chars = calloc(resource.len + 1, sizeof (char));
    memccpy(resource.chars, ascii_chars, 0, (resource.len + 1) * sizeof (char));
    return resource;
}

void
free_string(const string str) {
    free(str.chars);
}


/*
 * { Ресурс }
 * Владелец массива "zero-terminated" ASCII строк и колличества этих строк
 */
typedef struct {
    size_t count;
    string *restrict strs;
} strings_box;

strings_box
wrap_to_str_line_box(string *restrict strs, size_t count) {
    strings_box resource = {0};
    resource.strs = strs;
    resource.count = count;
    return resource;
}

void
print_str_line_box(const strings_box box) {
    for (size_t i = 0; i < box.count; ++i) {
        puts(box.strs[i].chars);
    }
}

void
free_str_line_box(strings_box box) {
    for (size_t i = 0; i < box.count; ++i) {
        free_string(box.strs[i]);
    }
}


/*
 * { Ресурс, Запись по указателю }
 * Временный владелец участка памяти для ASCII текста
 * Необходимо с помощью этого объекта обновлять буффер
 * Нельзя осовбождать память, нельзя использовать объект больше одного раза и после обновления данных буффера
 */
typedef struct {
    char *chars;
    size_t max_len;
} mut_string_slice;

/*
 * { Ресурс }
 * Опциональный владелец массива "zero-terminated" ASCII строк и колличества этих строк
 */
typedef struct
{
    strings_box _some;
} opt_strings_box;


opt_strings_box
opt_strings_box_none() {
    opt_strings_box opt_box = {0};
    opt_box._some.count = 0;
    opt_box._some.strs = NULL;
    return opt_box;
}

opt_strings_box
opt_strings_box_some(strings_box box) {
    opt_strings_box opt_box = {0};
    opt_box._some = box;
    return opt_box;
}

#ifdef NDEBUG
#define UNWRAP_STRING_BOX(OPT) _unwrap_strings_box_or_exit(OPT)
#else
#define UNWRAP_STRING_BOX(OPT) _unwrap_strings_box_or_exit(OPT, __FILE__, __LINE__)
#endif

#ifdef NDEBUG
const strings_box _unwrap_strings_box_or_exit(opt_strings_box opt) {
#else
strings_box _unwrap_strings_box_or_exit(opt_strings_box opt, const char *const _file, const int _line) {
#endif
    if (opt._some.count != 0) {
        return opt._some;
    } else {
#ifndef NDEBUG
        printf("[Error] Match empty value on opt_string_box, (%s:%d)\n", _file, _line);
#endif
        exit(EXIT_FAILURE);
    }
}

strings_box
unwrap_strings_box_or_default(opt_strings_box opt, strings_box default_value) {
    if (opt._some.count != 0) {
        return opt._some;
    } else {
        return default_value;
    }
}

void
unwrap_strings_box_or_call(opt_strings_box opt, void (*call)()) {
    if (opt._some.count == 0) {
        call();
    }
}

#endif // __STRINGS_H__
