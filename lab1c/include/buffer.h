#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

/*
 * { Ресурс, Изменяемый }
 * Владелец пополняемого буфера ASCII текста и расположение каретки в буффере
 * Может расширяться, обнуляться, выдавать копию строки из произвольного места в тексте
 */
typedef struct {
    char *restrict chars;
    size_t max_len;
    size_t cur_position;
} string_buffer;


string_buffer
create_string_buffer(size_t len) {
    string_buffer buff = {0};
    buff.chars = calloc(len, sizeof (char));
    buff.max_len = len;
    buff.cur_position = 0;
    return buff;
}

void
double_increase_string_buffer_size(string_buffer* mut_buff) {
    mut_buff->chars = realloc(mut_buff->chars, mut_buff->max_len * 2);
    mut_buff->max_len *= 2;
}

mut_string_slice
get_mut_string_slice(
        string_buffer* mut_buff,
        size_t min_len
        ) {
    while (mut_buff->max_len - mut_buff->cur_position < min_len) {
        double_increase_string_buffer_size(mut_buff);
    }

    mut_string_slice slice = {0};
    slice.chars = &mut_buff->chars[mut_buff->cur_position];
    slice.max_len = mut_buff->max_len - mut_buff->cur_position;
    return slice;
}

void
string_buffer_register_writed_data(
        string_buffer* mut_buff,
        size_t writed_chars_count
        ) {
    mut_buff->cur_position += writed_chars_count;
}

string
string_buffer_read_string(
        string_buffer buff,
        size_t len
        ) {
    size_t real_str_len = (buff.cur_position > len) ? len : buff.cur_position;
    char *restrict chars = calloc(real_str_len + 1, sizeof (char));
    memccpy(chars, buff.chars, 0, real_str_len);
    chars[real_str_len] = '\0';
    return wrap_to_string(chars, real_str_len + 1);
}

void
string_buffer_clear(
        string_buffer* mut_buff
        ) {
    mut_buff->cur_position = 0;
}

void
string_buffer_clear_for(
        string_buffer* mut_buff,
        size_t position
        ) {
    if (position >= mut_buff->cur_position) {
        string_buffer_clear(mut_buff);
    } else if (position == 0 || mut_buff->cur_position == 0) {
        // Do nothing!
        return;
    } else {
        size_t new_buff_position = mut_buff->cur_position - position;
        register char *from = &mut_buff->chars[position];
        register char *to = mut_buff->chars;
        do {
            register ssize_t n = (new_buff_position + 7) / 8;
            switch (new_buff_position % 8) {
            case 0: do {    *to++ = *from++;
            case 7:         *to++ = *from++;
            case 6:         *to++ = *from++;
            case 5:         *to++ = *from++;
            case 4:         *to++ = *from++;
            case 3:         *to++ = *from++;
            case 2:         *to++ = *from++;
            case 1:         *to++ = *from++;
                        } while (--n > 0);
            }
        } while(0);
    }
}

void
free_string_buffer(
        string_buffer buff
        ) {
    free(buff.chars);
}

#endif // __BUFFER_H__
