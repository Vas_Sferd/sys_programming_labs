#ifndef __USIZE_BOX_H__
#define __USIZE_BOX_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <strings.h>

/*
 * { Ресурс }
 * Владелец массива целых беззнаковых чисел и колличества этих строк.
 */
typedef struct {
    size_t *restrict nums;
    size_t count;
} usize_array_box;


usize_array_box
wrap_to_usize_array_box(size_t *restrict nums, size_t count) {
    usize_array_box resource = {0};
    resource.nums = nums;
    resource.count = count;
    return resource;
}

static int
usize_cmp(const size_t *v1, const size_t *v2) {
    return (v1 > v2) ? 1 : -1;
}

usize_array_box
sort_usize_array_box(usize_array_box arr) {
    size_t *restrict nums = calloc(arr.count, sizeof (size_t));
    memccpy(nums, arr.nums, 0, arr.count * sizeof (size_t));
    qsort(nums, arr.count, sizeof (size_t), (int (*)(const void*, const void*))usize_cmp);
    return wrap_to_usize_array_box(nums, arr.count);
}

usize_array_box
parse_from_string(strings_box strs) {
    size_t *restrict nums = calloc(strs.count, sizeof (size_t));
    for (size_t i = 0; i < strs.count; ++i) {
        nums[i] = (size_t)(unsigned)atoi(strs.strs[i].chars);
    }
    return wrap_to_usize_array_box(nums, strs.count);
}

void
free_usize_array_box(usize_array_box box) {
    free(box.nums);
}

#endif
